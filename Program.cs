﻿using System;

namespace MaxMultiple
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        int maxMultiple(int divisor, int bound) {
            int maxMult = 0;
            for(int i = bound / 2; i > 0; i--){
                if((divisor * i) > maxMult && (divisor * i) <= bound){
                    maxMult = divisor * i;
                }
            }
            
            return maxMult;
        }

    }
}
